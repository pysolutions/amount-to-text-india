# -*- coding: utf-8 -*-
##############################################################################
#    
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2009 Tiny SPRL (<http://tiny.be>).
#    Forked by PY Solutions.
#
##############################################################################

import logging
from translate import _

_logger = logging.getLogger(__name__)

#-------------------------------------------------------------
#ENGLISH
#-------------------------------------------------------------

TO_19 = ( 'Zero',  'One',   'Two',  'Three', 'Four',   'Five',   'Six',
          'Seven', 'Eight', 'Nine', 'Ten',   'Eleven', 'Twelve', 'Thirteen',
          'Fourteen', 'Fifteen', 'Sixteen', 'Seventeen', 'Eighteen', 'Nineteen',
)
TENS  = ( 'Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy', 'Eighty', 
        'Ninety',
)
DENOM = ( '', 'Thousand', 'Lakh', 'Crore', 'Arab', 'Kharab', 'Neel', 'Padm', 
        'Shankh', 'NA',
)

def _convert_nn(val):
    """
    Convert a value < 100 to English.
    
    :param val: 2 digit Value into Words 
    :return word: A string containing the amount in words
    """
    if val < 20:
        return TO_19[val]
    for (dcap, dval) in ((k, 20 + (10 * v)) for (v, k) in enumerate(TENS)):
        if dval + 10 > val:
            if val % 10:
                return dcap + '-' + TO_19[val % 10]
            return dcap

def _convert_nnn(val):
    """
    Convert a value < 1000 to english, special cased because it is the 
    level that kicks off the < 100 special case.  The rest are more general.
    This also allows you to get strings in the form of 'forty-five hundred' 
    if called directly.
    
    :param val: 3 digit Value into Words 
    :return word: A string containing the amount in words
    """
    word = ''
    (mod, hundreds) = (val % 100, val / 100)
    if hundreds > 0:
        word = TO_19[hundreds] + ' Hundred'
        if mod > 0:
            word = word + ' and '
    if mod > 0:
        word = word + _convert_nn(mod)
    return word


def english_number(val): # 3,96,445
    if val < 100:
        return _convert_nn(val)
    if val < 1000:
         return _convert_nnn(val)
    #for (didx, dval, dchar) in ((v, 100 ** v, DENOM[v]) for v in range(len(DENOM))):
    val1 = 1000
    for (didx, dval) in ((v - 1, 100 ** v *10) for v in range(len(DENOM))):
        if dval > val:
            mod = 100 ** didx * 10
            l = val // mod
            r = val - (l * mod)
            ret = _convert_nnn(l) + ' ' + DENOM[didx]
            if r > 0:
                ret = ret + ', ' + english_number(r)
            return ret

def amount_to_text(number, currency):
    number = '%.2f' % number
    units_name = currency
    list = str(number).split('.')
    start_word = english_number(int(list[0]))
    end_word = english_number(int(list[1]))
    paisa_number = int(list[1])
    paisa_name = (paisa_number > 1) and 'Paise' or 'Paisa'
    final_result = units_name + ' ' + start_word + ' and ' + end_word + ' ' + \
        paisa_name
    return final_result


#-------------------------------------------------------------
# Generic functions
#-------------------------------------------------------------

_translate_funcs = {'en' : amount_to_text, 'hi':amount_to_text}
    
def amount_to_text(nbr, lang='en', currency='Rupees'):
    """ 
    Converts an integer to its textual representation, using the language set in
    the context if any.
    
    Example::
        
    1654.87: Rupees One thousand, Six Hundread and Fifty-Four Eighty-Seven Paise
    """
    import openerp.loglevels as loglevels
    
    if not _translate_funcs.has_key(lang):
        _logger.warning(_("no translation function found for lang: '%s'"), lang)
        lang = 'en'
    return _translate_funcs[lang](abs(nbr), currency)

if __name__=='__main__':
    from sys import argv
    
    lang = 'nl'
    if len(argv) < 2:
        for i in range(1,200):
            print i, ">>", int_to_text(i, lang)
        for i in range(200,999999,139):
            print i, ">>", int_to_text(i, lang)
    else:
        print int_to_text(int(argv[1]), lang)


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
